let player = {
	name: 'Ash Ketchum',
	age: 10,
	
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},


	pokemon: [
		'Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur' ], 

	
	talk: function(number){
			console.log(player.pokemon[number] +'! I choose you!');
		}  
}

console.log(player);


console.log('Result of dot notation: ');
console.log(player.name);
console.log('Result of bracket notation: ');
console.log(player.pokemon);
console.log('Result of talk method');
console.log(player.talk(0));


let Pikachu = new Pokemon("Pikachu", 12);
let Venosaur = new Pokemon('Venosaur', 8);
let Dragonite = new Pokemon('Dragonite', 17);

function Pokemon(name, level) {

     
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = level;

      
      this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          target.health -= this.attack;
          console.log( target.name + "'s health is now reduced to " + target.health);
          if(target.health < 1){
         console.log(target.name + ' fainted.');
      	}


          };
      
}

 

console.log(Pikachu);
console.log(Venosaur);
console.log(Dragonite);


Dragonite.tackle(Pikachu);
console.log(Pikachu);
Dragonite.tackle(Pikachu);

Venosaur.tackle(Dragonite);
console.log(Dragonite);
Dragonite.tackle(Venosaur);

